package com.example.andreiman.scdapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.support.v4.content.ContextCompat.getSystemService;

public class DataSender extends AsyncTask<String, String, String> {

    static double latitude, longitude;
    @Override
    protected String doInBackground(String... strings) {

        OkHttpClient client = new OkHttpClient();


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
//        RequestBody body = RequestBody.create(JSON, "{\n" +
//                "    \"latitudine\": \""+strings[0]+"\",\n" +
//                "    \"longitudine\": \""+strings[1]+"\",\n" +
//                "    \"timestamp\": \""+dateFormat.format(date)+"\",\n" +
//                "    \"user\": \""+strings[2]+"\"\n" +
//                "}");
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("latitudine",strings[0]);
            postdata.put("longitudine",strings[1]);
            postdata.put("timestamp",dateFormat.format(date));

            postdata.put("user",strings[2]);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(JSON, postdata.toString());
//        RequestBody body = new FormBody.Builder()
//
//                .add("latitudine", strings[0])
//                .add("longitudine", strings[1])
//                .add("timestamp", dateFormat.format(date))
//                .add("user", strings[2])
//                .build();

        Request request = new Request.Builder()
                .url("http://andrei-man.ro/api/locatii/")
                .post(body)
//                .get()
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("Authorization","Token 5ca6645451ebe33481dcbbf253b5003d74c2308a")
                .build();
        try {
            Response response = client.newCall(request).execute();
        int a=2;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ok";
    }
}